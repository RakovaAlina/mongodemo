package mongo.crud.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Order {

	@Id
	private Long id;
	private Item item;
	private int amount;
	private Date date;
	private String customerName;

	public Order() {
	}

	public Order(Long id, Item item, int amount, Date date, String customerName) {
		this.id = id;
		this.item = item;
		this.amount = amount;
		this.date = date;
		this.customerName = customerName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}