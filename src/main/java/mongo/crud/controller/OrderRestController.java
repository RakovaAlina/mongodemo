package mongo.crud.controller;

import java.util.List;

import mongo.crud.dao.OrderRepository;
import mongo.crud.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
public class OrderRestController {

	
	@Autowired
	private OrderRepository orderRepository;

	
	@GetMapping()
	public List getOrders() {
		return orderRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Order> getOrder(@PathVariable("id") Long id) {

		Order orderWithId = new Order();
		orderWithId.setId(id);
		Order order = orderRepository.find(orderWithId);
		if (order == null) {
			return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Order>(order, HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<Order> createOrder(@RequestBody Order order) {

		orderRepository.create(order);

		return new ResponseEntity<Order>(order, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Long> deleteOrder(@PathVariable Long id) {

		Order orderWithId = new Order();
		orderWithId.setId(id);
		orderRepository.delete(orderWithId);

		return new ResponseEntity<Long>(id, HttpStatus.OK);

	}

	@PutMapping("/{id}")
	public ResponseEntity<Order> updateOrder(@PathVariable Long id, @RequestBody Order order) {

		orderRepository.update(order);

		return new ResponseEntity<Order>(order, HttpStatus.OK);
	}

}