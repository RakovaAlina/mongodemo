package mongo.crud.dao;

import java.util.List;

import com.mongodb.WriteResult;
import mongo.crud.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepository {

	@Autowired
	MongoTemplate mongoTemplate;

	final String COLLECTION = "orders";

	public void create(Order order) {
		mongoTemplate.insert(order);
	}

	public void update(Order order) {
		mongoTemplate.save(order);
	}

	public void delete(Order order) {
		mongoTemplate.remove(order);
	}

	public void deleteAll() {
		mongoTemplate.remove(new Query(), COLLECTION);
	}

	public Order find(Order order) {
		Query query = new Query(Criteria.where("_id").is(order.getId()));
		return mongoTemplate.findOne(query, Order.class, COLLECTION);
	}

	public List<Order> findAll() {
		return (List <Order> ) mongoTemplate.findAll(Order.class);
	}

}